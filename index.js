
/* ----------Bài 1 ---------- */
function ketQua(){
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;
    var diemMon1 = document.getElementById("txt-mon-1").value*1;
    var diemMon2 = document.getElementById("txt-mon-2").value*1;
    var diemMon3 = document.getElementById("txt-mon-3").value*1;
    var diemKhuVuc = khuVuc();
    var diemDoiTuong = doiTuong();
    if(diemChuan > 30 || diemChuan == 0){
        alert("Vui lòng kiểm tra lại điểm chuẩn!");
    }
    if((diemMon1 <= 0) || (diemMon2 <= 0) || (diemMon3 <= 0)){
        var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`Bạn đã rớt. Do có điểm nhỏ hơn bằng 0`);
    }
    else{
        if(diemMon1>10 || diemMon2>10||diemMon3>10 || diemChuan == 0){
            alert("Bạn vui lòng kiểm tra lại điểm đã nhập!");
            var result1El = document.getElementById("txt-hien-thi").setAttribute('value',``);
        }
        else{
            var tongDiem = diemMon1+diemMon2+diemMon3+diemKhuVuc+diemDoiTuong;
            if(tongDiem >= diemChuan){
                var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`Bạn đã đậu. Tổng điểm: ${tongDiem}`);
            }
            else{
                var result1El = document.getElementById("txt-hien-thi").setAttribute('value',`Bạn đã rớt. Tổng điểm: ${tongDiem}`);
            }
        }
    }


}

function khuVuc(){
    var kiemTraKhuVuc = document.getElementById("select-location").value;
    var checkLc;
    if(kiemTraKhuVuc == "A")
        checkLc = 2;
    else if(kiemTraKhuVuc == "B")
        checkLc = 1;
    else if(kiemTraKhuVuc == "C")
        checkLc = 0.5;
    else
        checkLc = 0;
    return checkLc;
}

function doiTuong(){
    var kiemTraDoiTuong = document.getElementById("select-object").value;
    var checkObj;
    if(kiemTraDoiTuong == "1")
        checkObj = 2.5;
    else if(kiemTraDoiTuong == "2")
        checkObj = 1.5;
    else if(kiemTraDoiTuong == "3")
        checkObj = 1;
    else
        checkObj = 0;
    return checkObj;
}

/* ----------Bài 2 ---------- */

function tinhTienDien(){
    var hoTen = document.getElementById("txt-ten").value;
    var soKW = document.getElementById("txt-so-kw").value*1;
    var tongTienEl = new Intl.NumberFormat('vn-VN', {style: 'currency', currency: "VND"}).format(tinhKW(soKW));
    var result2El = document.getElementById("txt-tinh").setAttribute('value',`Tổng tiền ${hoTen} phải thanh toán: ${tongTienEl}`);
}

function tinhKW(soKW){
    var tienTra = 0;
    if(soKW <= 50)
        tienTra = soKW*500;
    else if(soKW > 50 && soKW <= 100)
        tienTra = 500*50+(soKW-50)*650;
    else if(soKW >=100 && soKW < 200)
        tienTra = 500*50 + 650*50 + (soKW-100)*850;
    else if(soKW >= 200 && soKW < 350)
        tienTra = 500*50 + 650*50 + 850*100+ (soKW - 200)*1100;
    else 
        tienTra = 500*50+650*50+850*100+1100*150+(soKW-350)*1300;
    return tienTra;
}



function bai1(){
    document.getElementById("e1").style.display = 'block';
    document.getElementById("e2").style.display = "none";
}

function bai2(){
    document.getElementById("e2").style.display = 'block';
    document.getElementById("e1").style.display = "none";
}
